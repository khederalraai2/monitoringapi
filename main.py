from flask import Flask, jsonify # python framework
import time
import psutil  # (python system and process utilities) cross-platform library for retrieving information on running processes and system utilization
from datetime import datetime # classes for manipulating dates and times.
from flask_cors import CORS, cross_origin # extension for handling Cross Origin Resource Sharing (CORS), making cross-origin AJAX possible.






# Run all function every 0.5 second
while True:
# Function to get ram value
  def get_ram_total():
    
    return (psutil.virtual_memory().percent)





# Function to get cpu value
  def get_cpu_percent():
    return (psutil.cpu_percent())

# Function to get datetime
  def get_Datum():  
    now = datetime.now()
    return (now.strftime("%d/%m/%Y %H:%M:%S"))

# Function to get process Number
  def get_process():
        
     new_list = []
     for index, magician in enumerate(psutil.process_iter()):
         new_list.append(index)
         
     return new_list.pop()



  # Create API with flask 
  app = Flask(__name__)
  cors = CORS(app)
  app.config['CORS_HEADERS'] = 'Content-Type'
  @app.route('/', methods=['GET'])
  @cross_origin()
  
  def index():

      return jsonify(
          {
              "ram": {  
              'ramTotal': get_ram_total(),
              'Datum': get_Datum()
              },
              "cpu": {  
              'auslastung': get_cpu_percent(),
              'Datum': get_Datum()
              },
              "process": {  
              'auslastung':get_process()+1,
              'Datum': get_Datum()
              }
          }
  )

  app.run()

  time.sleep(0.5)

 